export const routes = [
  {
    path:"/",
    component:() => import ("../layouts/MainLayout.vue"),
    children:[
      {
        path:"",
        component:() => import ("../pages/InPage.vue"),
      },
      {
        path:"map",
        component:() => import ("../pages/Map.vue"),
      }
    ]
  }
]