import { defineStore } from "pinia";
import { darkTheme, lightTheme } from "naive-ui";
import type { GlobalTheme } from "naive-ui";
import axios from "axios";
import moment from "moment";
export const CheckDay = (day: number) => {
  const d = new Date();
  if (day + d.getDay() > 6) {
    return day + d.getDay() - 7;
  } else {
    return day + d.getDay();
  }
};
export const useWeatherStore = defineStore("weather", {
  state: () => {
    return {
      weather: null,
      bgcolor: true,
      city_name: "Krasnodar",
      city: null,
    };
  },

  getters: {
    getcoord(state){
      return state.city && state.city.coord ? state.city.coord : []
    },
    getcolor(state) {
      return state.bgcolor === false ? lightTheme : darkTheme;
    },
  },

  // actions: {
  //   async api_weather(query = 'Krasnodar') {
  //     const options: Record<string, unknown> = {
  //       method: 'GET',
  //       url: 'https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily',
  //       params: { city: query },
  //       headers: {
  //         'X-RapidAPI-Key': '69acf0d401msh3e04a18cef9d7c8p1539c0jsn23457215ba70',
  //         'X-RapidAPI-Host': 'weatherbit-v1-mashape.p.rapidapi.com',
  //       },
  //     };

  //     const list = await axios.request(options);
  //     this.weather = list.data
  //     console.log(list)
  //   },
  // },

  actions: {
    async api_weather() {
      const key = "75a9c4ce5372fc91240d58d0807b30d7";
      const baseURL = `https://api.openweathermap.org/data/2.5/forecast?q=${this.city_name}&appid=${key}&units=metric`;
      const list = await axios.get(baseURL);
      const datemoment = moment()
      const listDate: Record<string, any> = [];
      const monthPlus1 = datemoment.get('month') + 1;
      this.city=list.data.city
      for (let i = 0; i < 5; i++) {
        if (i != 0) datemoment.set('date', datemoment.get('date') + 1);
        listDate.push(
          moment(`${datemoment.get('year')}-${monthPlus1}-${datemoment.get('date')}`).format('YYYY-MM-DD')
        );
      }
      const dateFilter = (value: Record<string, any>) => {
        for (let g = 0; g < 5; g++) {
          if (
            value.dt_txt.split(" ")[0] === listDate[g] &&
            value.dt_txt.split(" ")[1] === "12:00:00"
          )
            return value;
        }
      };
      const data = list.data.list.filter(dateFilter);
      this.weather = data;
    },

    setCityName(query: string) {
      this.city_name = query;
    },
  },
});
